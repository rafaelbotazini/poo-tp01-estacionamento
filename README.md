# Estacionamento 
## POO - Trabalho Prático 1 - Valor: 10 pontos

Dado o volume crescente de automóveis circulando nas metrópoles brasileiras, uma das situações mais comuns no dia a dia dos motoristas é utilizar estacionamentos privados quando se deslocam às áreas centrais das cidades utilizando seus veículos particulares.

Os estacionamentos se proliferaram nos últimos anos e, com o acúmulo de dados e surgimento de diferentes regras de uso, torna-se útil utilizar um sistema de informação para automatizar o processo e fornecer informações a operadores do sistema e gerentes dos estabelecimentos.

Seu grupo de trabalho, composto por até 4 pessoas, ficou responsável por desenvolver um sistema que deve resolver a seguinte situação:

* Um estacionamento tem um número limitado de vagas. Um automóvel só podeestacionar ali caso haja vagas.
* O sistema deve manter registros de quando e por quanto tempo cada um dosautomóveis ficaram estacionados.
* Deve ser possível emitir o valor a ser pago por um automóvel baseado na regra decobrança especificada neste documento.
* Um proprietário de automóvel pode pedir um extrato de estacionamento, verificandoos registros de seus usos de estacionamento, o preço pago em cada um e o valor total gasto em estacionamentos.
* Um estacionamento precisa emitir seu faturamento total até o momento. 

Os gerentes da rede de estacionamentos que contratou o serviço de vocês e ainda solicitou que o sistema seja desenvolvido utilizando técnicas modernas de programação, que possibilitem futuras melhorias e que tenha uma interface gráfica de usuário atraente, para facilitar o rápido manejo por parte de quem trabalha nos estabelecimentos.
**Até o momento**, as tarefas do seu grupo de trabalho, que deve ser composto por até quatro pessoas, são as seguintes:

1. Modelar e criar as classes para resolver o problema proposto;
2. Criar um programa para resolver o problema de acordo com o especificado;
3. Garantir que o programa seja ***robusto***, suportando erros comuns sem terminar sua operação.
4. Gravar em um arquivo texto e depois recuperar informações sobre estacionamentos, carros e utilizações

Regras de cobrança
------------------

Os clientes pagam R$9,00 por hora utilizada e frações proporcionais;

Requisitos adicionais
---------------------

Os sócios já trabalharam com outros desenvolvedores anteriormente e desejam que ***sejam utilizados os princípios básicos de programação orientada por objetos***, ou seja, o programa deve ser implementado baseado em classes e objetos.

###Prazos

O sistema deve ser desenvolvido em etapas, para melhor supervisão do desenvolvimento do sistema. Para a primeira etapa, foi acertado que vocês têm até o dia **12/05/2018 09:00** para entregar o sistema especificado. A apresentação do produto funcional está marcada para o mesmo dia as 11:10.

###Regras de pontuação

|Item | Pontuação |
| --- | --- |
|Modelagem correta das classes respeitando os princípios desejáveis de Programação Orientada por Objetos: | 30% |
|Codificação Correta, respeitando os requisitos estabelecidos na especificação: | 40% |
|Sistema robusto, tolerante a falhas: | 20% |
|Interface de boa qualidade e fácil usabilidade: | 10% |
|Gravação em arquivo texto: | 2 pontos extras. |


Observações 
-----------

* **IMPORTANTE**: Esta especificação de trabalho vai evoluir à medida que outros assuntos forem abordados. Fique atento aos prazos, pois eventuais atrasos podem impactar nas entregas futuras.
